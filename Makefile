all: build-local
test: lint unit-test

PLATFORM=local
TAG=worldofmusic

.PHONY: build-local
build-local:
	@docker build . --target bin \
	--output bin/${PLATFORM}/ \
	--platform ${PLATFORM} \
	-t ${TAG}

.PHONY: build
build:
	@docker build . --target bin \
	--platform ${PLATFORM} \
	-t ${TAG}

.PHONY: unit-test
unit-test:
	@docker build . --target unit-test

.PHONY: unit-test-coverage
unit-test-coverage:
	@docker build . --target unit-test-coverage \
	--output coverage/
	cat coverage/cover.out

.PHONY: lint
lint:
	@docker build . --target lint
