#!/bin/bash

set -o xtrace

RELEASE_NAME="$1"
TAG_NAME="$2"
PROJECT_ID="$3"
PRIVATE_TOKEN="$4"

if [ "$4" == "" ]; then
    echo "Missing parameter! Parameters are RELEASE_NAME, TAG_NAME, PROJECT_ID, and PRIVATE_TOKEN.";
    exit 1;
fi

apk add --no-cache make curl jq
curl --location --output ./release-cli "https://release-cli-downloads.s3.amazonaws.com/latest/release-cli-linux-amd64"
chmod +x ./release-cli

mkdir bin

make build-local PLATFORM=darwin/amd64
darwin_url=$(curl --request POST --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" \
     --form "file=@bin/darwin/amd64/worldofmusic" "https://gitlab.com/api/v4/projects/$PROJECT_ID/uploads" | jq -r '.full_path')

make build-local PLATFORM=darwin/arm64
darwin_arm_url=$(curl --request POST --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" \
     --form "file=@bin/darwin/arm64/worldofmusic" "https://gitlab.com/api/v4/projects/$PROJECT_ID/uploads" | jq -r '.full_path')

make build-local PLATFORM=windows/amd64
windows_amd64_url=$(curl --request POST --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" \
     --form "file=@bin/windows/amd64/worldofmusic.exe" "https://gitlab.com/api/v4/projects/$PROJECT_ID/uploads" | jq -r '.full_path')

make build-local PLATFORM=linux/amd64
linux_amd64_url=$(curl --request POST --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" \
     --form "file=@bin/linux/amd64/worldofmusic" "https://gitlab.com/api/v4/projects/$PROJECT_ID/uploads" | jq -r '.full_path')

make build-local PLATFORM=linux/arm
linux_arm_url=$(curl --request POST --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" \
     --form "file=@bin/linux/arm/worldofmusic" "https://gitlab.com/api/v4/projects/$PROJECT_ID/uploads" | jq -r '.full_path')

echo "Path Darwin executable: ""$darwin_url"
echo "Path Darwin arm(m1) executable: ""$darwin_arm_url"
echo "Path Windows executable: ""$windows_amd64_url"
echo "Path Linux arm executable: ""$linux_arm_url"
echo "Path Linux executable: ""$linux_amd64_url"


./release-cli create --name "$RELEASE_NAME" \
--tag-name "$TAG_NAME" \
--assets-link '{"name":"Darwin","url":"https://gitlab.com'"$darwin_url"'"}' \
--assets-link '{"name":"Darwin-ARM (M1 chip)","url":"https://gitlab.com'"$darwin_arm_url"'"}' \
--assets-link '{"name":"Linux-AMD64","url":"https://gitlab.com'"$linux_amd64_url"'"}' \
--assets-link '{"name":"Linux-ARM","url":"https://gitlab.com'"$linux_arm_url"'"}' \
--assets-link '{"name":"Windows-AMD64","url":"https://gitlab.com'"$windows_amd64_url"'"}'
