package release

import "encoding/xml"

// Releases defines the schema of the delivered releases.
type Releases struct {
	XMLName  xml.Name  `xml:"matchingReleases"`
	Releases []Release `xml:"release"`
}

type Release struct {
	XMLName    xml.Name `xml:"release"`
	Name       string   `xml:"name"`
	TrackCount int      `xml:"trackCount"`
}
