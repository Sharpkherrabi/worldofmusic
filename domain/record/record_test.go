package record

import (
	"encoding/xml"
	"gitlab.com/Sharpkherrabi/worldofmusic/domain/release"
	"reflect"
	"testing"
	"time"
)

func records() RecordsList {
	t, _ := time.Parse("2006.01.02", "2002.05.24")
	r1 := Record{
		Title:       "Good Vibes",
		Name:        "Good Vibes Summer",
		Genre:       "HipHop",
		ReleaseDate: t,
		Label:       "Luxe",
		Formats:     []string{"CD", "Cassette", "DVD"},
		Tracks:      []string{"Take me to church", "Blonde", "Walking in your heart"},
	}
	t, _ = time.Parse("2006.01.02", "2002.05.13")
	r2 := Record{
		Title:       "Walking in the ocean",
		Name:        "Walking in the ocean",
		Genre:       "Techno",
		ReleaseDate: t,
		Label:       "Alfa",
		Formats:     []string{"CD"},
		Tracks:      []string{"Alberta", "Mona Lisa"},
	}
	return RecordsList{r1, r2}
}

func releases() release.Releases {
	return release.Releases{
		XMLName: xml.Name{},
		Releases: []release.Release{
			{
				XMLName:    xml.Name{},
				Name:       "Good Vibes Summer",
				TrackCount: 3,
			},
			{
				XMLName:    xml.Name{},
				Name:       "Walking in the ocean",
				TrackCount: 2,
			},
		},
	}
}

func TestRecordsList_ToReleases(t *testing.T) {
	var tests = []struct {
		input RecordsList
		want  release.Releases
	}{
		{
			records(),
			releases(),
		},
	}

	for _, test := range tests {
		if got := test.input.ToReleases(); !reflect.DeepEqual(got, test.want) {
			t.Errorf("got %+v, \nwant %+v", got, test.want)
		}
	}
}
