package record

import (
	"encoding/xml"
	"reflect"
	"testing"
	"time"
)

func xmlRecords() XMLRecords {
	return XMLRecords{
		XMLName: xml.Name{},
		Records: []record{
			{
				XMLName:      xml.Name{},
				Title:        "Alpacino",
				Name:         "Alpacino laracoun",
				Genre:        "Dance",
				ReleaseDate:  "2007.05.24",
				Label:        "Delicious",
				Formats:      "CD, Chip, DVD",
				TrackListing: []string{"track1", "track2"},
			},
			{
				XMLName:      xml.Name{},
				Title:        "Lacoronia",
				Name:         "Lacoronia Digton",
				Genre:        "R&B",
				ReleaseDate:  "2008.06.22",
				Label:        "Alcatra",
				Formats:      "CD, DVD, Cassette",
				TrackListing: []string{"track1", "track3"},
			},
		},
	}
}

func recordsList() []Record {
	releaseDate, _ := time.Parse("2006.01.02", "2007.05.24")
	r1 := Record{
		Title:       "Alpacino",
		Name:        "Alpacino laracoun",
		Genre:       "Dance",
		ReleaseDate: releaseDate,
		Label:       "Delicious",
		Formats:     []string{"CD", "Chip", "DVD"},
		Tracks:      []string{"track1", "track2"},
	}
	releaseDate, _ = time.Parse("2006.01.02", "2008.06.22")
	r2 := Record{
		Title:       "Lacoronia",
		Name:        "Lacoronia Digton",
		Genre:       "R&B",
		ReleaseDate: releaseDate,
		Label:       "Alcatra",
		Formats:     []string{"CD", "DVD", "Cassette"},
		Tracks:      []string{"track1", "track3"},
	}
	return []Record{r1, r2}
}

func TestXMLRecords_ToRecords(t *testing.T) {
	var tests = []struct {
		input XMLRecords
		want  []Record
	}{
		{
			xmlRecords(),
			recordsList(),
		},
	}

	for _, test := range tests {
		if got := test.input.ToRecords(); !reflect.DeepEqual(got, test.want) {
			t.Errorf("got %+v, \nwant %+v", got, test.want)
		}
	}
}
