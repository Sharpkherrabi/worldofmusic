package record

import (
	"encoding/xml"
	"strings"
	"time"
)

// XMLRecords defines the schema of the xml files.
type XMLRecords struct {
	XMLName xml.Name `xml:"records"`
	Records []record `xml:"record"`
}

type record struct {
	XMLName      xml.Name `xml:"record"`
	Title        string   `xml:"title"`
	Name         string   `xml:"name"`
	Genre        string   `xml:"genre"`
	ReleaseDate  string   `xml:"releasedate"`
	Label        string   `xml:"label"`
	Formats      string   `xml:"formats"`
	TrackListing []string `xml:"tracklisting>track"`
}

// ToRecords convert a XMLRecords representation to a list of Record-s.
func (r XMLRecords) ToRecords() []Record {
	var records []Record
	layout := "2006.01.02"
	for _, xmlRecord := range r.Records {
		releaseDate, err := time.Parse(layout, xmlRecord.ReleaseDate)
		if err != nil {
			// skip records with invalid date. (Example => Electrapop, "1998.09.31")
			continue
		}
		formats := strings.Split(xmlRecord.Formats, ",")
		for i := range formats {
			formats[i] = strings.TrimSpace(formats[i])
		}
		record := Record{
			Title:       xmlRecord.Title,
			Name:        xmlRecord.Name,
			Genre:       xmlRecord.Genre,
			ReleaseDate: releaseDate,
			Label:       xmlRecord.Label,
			Formats:     formats,
			Tracks:      xmlRecord.TrackListing,
		}
		records = append(records, record)
	}
	return records
}
