package record

import (
	"gitlab.com/Sharpkherrabi/worldofmusic/domain/release"
	"time"
)

// Record is the representation of a record in the business layer.
type Record struct {
	Title       string
	Name        string
	Genre       string
	ReleaseDate time.Time
	Label       string
	Formats     []string
	Tracks      []string
}

// RecordsList is a type with a slice of Record-s as source type.
type RecordsList []Record

// ToReleases convert a list of Record-s (type RecordsList) to a list of releases (type release.Releases).
func (r RecordsList) ToReleases() release.Releases {
	var releases release.Releases
	for _, rel := range r {
		releases.Releases = append(releases.Releases, release.Release{
			Name:       rel.Name,
			TrackCount: len(rel.Tracks),
		})
	}
	return releases
}
