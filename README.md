## World Of Music
**worldofmusic** is a RESTful application which takes an XML document as a source of data and provides filtering functions.
#### Paths:
* `/filter1`: returns a list of releases with more than 10 tracks and a release date before 01/01/2001.
* `/filter2`: returns a list of releases with at least 2 Compact Discs.

### Enable BUILDKIT
To leverage BuildKit features like binding the build context without using `COPY` which creates an extra layer in the container image, you must set the environment variable `DOCKER_BUILDKIT` to *1*.
```bash
$ export DOCKER_BUILDKIT=1
```

### Build
You can build the Go binary by running:
```bash
$ make build-local
```
The command will automatically build the binary for the platform and OS where the command has been run and create a folder `bin/` where the executable resides.

If you want to build for another platform. For instance, you are using `linux/amd64`, and you want to build for `windows/amd64`:
```bash
$ make build-local PLATFORM=windows/amd64
```

### Lint
This project uses [golangci-lint](https://github.com/golangci/golangci-lint) to lint the code. To run the linter locally:
```bash
$ make lint
```
Each commit to the master branch triggers the `lint-test` job.

### CI/CD
This repository has Gitlab CI/CD enabled. All commits to the master branch trigger `lint-test`.\
For tagged commits, additional jobs are triggered: `dockerize`, `release` and `deploy`.\
In the `dockerize` job, an image is created and then pushed to the Gitlab Container Registry.\
The `release` job uploads the prebuilt Go binaries and creates a new [release](https://gitlab.com/Sharpkherrabi/worldofmusic/-/releases) .\
The `deploy` job uses `Helm` to deploy the Chart defined in `./deploy/helm` to an EKS cluster.

### Time
It took me roughly 16 hours to architect the application and write the code. The overall time is 25 hours.

### Upcoming features
If the structure of the source XML data changes, we only need to modify the XML struct `XMLRecords` and `ToRecords` method in the file `domain/record/record_xml.go`. The code of the business layer is unaffected.\
If we want to use an API to get the data instead of a file on disk, we need to change the `repository` layer in `record/repository/xml/xml.go`. The `use case` and `delivery` layers will not change.\
If we want to introduce a new filter, we need to add a method to the `recordUseCase` struct in `record/usecase/usecase.go` with a type `func()(release.Releases, error)`. Additionally, we need to add the method signature to the interface in the delivery layer.