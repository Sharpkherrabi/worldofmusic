package http

import (
	"encoding/xml"
	"errors"
	"fmt"
	"github.com/labstack/echo/v4"
	"gitlab.com/Sharpkherrabi/worldofmusic/domain/release"
	"net/http"
	"net/http/httptest"
	"regexp"
	"testing"
)

type recordUCase struct{}

var releases = release.Releases{
	XMLName: xml.Name{},
	Releases: []release.Release{
		{
			XMLName:    xml.Name{},
			Name:       "release1",
			TrackCount: 6,
		},
		{
			XMLName:    xml.Name{},
			Name:       "release2",
			TrackCount: 8,
		},
	},
}

func (r recordUCase) ReleaseFilter1() (release.Releases, error) {
	return releases, nil
}
func (r recordUCase) ReleaseFilter2() (release.Releases, error) {
	return releases, nil
}

var xmlResponse = `
<?xml version="1.0" encoding="UTF-8"?>
<matchingReleases>
	<release>
		<name>release1</name>
		<trackCount>6</trackCount>
	</release>
	<release>
		<name>release2</name>
		<trackCount>8</trackCount>
	</release>
</matchingReleases>`

type routesSchema struct {
	path   string
	method string
}

var registeredRoutes = []routesSchema{
	{
		path:   "/filter1",
		method: echo.GET,
	},
	{
		path:   "/filter2",
		method: echo.GET,
	},
}

var nonCharacterMatch = regexp.MustCompile(`[\t\n\r]+`)

func TestCheckRegisteredRoutes(t *testing.T) {
	e := echo.New()
	NewReleaseHandler(e, recordUCase{})
	routes := e.Routes()
	for _, registeredRoute := range registeredRoutes {
		if !checkRouteContains(registeredRoute.method, registeredRoute.path, routes) {
			t.Errorf("no handler is registred for path %s with method %s",
				registeredRoute.path, registeredRoute.method)
		}
	}
}

func TestHandlerFromFilter(t *testing.T) {
	e := echo.New()
	// The path and method are irrelevant.
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	// We are here testing handlerFromFilter so using ReleaseFilter1 or ReleaseFilter2 is irrelevant.
	err := handlerFromFilter(recordUCase{}.ReleaseFilter1)(c)
	if err != nil {
		t.Errorf("error occurred while trying to write to req: %v", err)
	}
	if rec.Code != http.StatusOK {
		t.Errorf("returned a status != %d", http.StatusOK)
	}
	got := nonCharacterMatch.ReplaceAllString(rec.Body.String(), "")
	expected := nonCharacterMatch.ReplaceAllString(xmlResponse, "")
	if got != expected {
		t.Errorf("response (%s) does not match the expected xml Response %s", got, expected)
	}
}

type recordUCaseError struct{}

var errorMessageFilter = "error from ReleaseFilter"

func (r recordUCaseError) ReleaseFilter1() (release.Releases, error) {
	return release.Releases{}, errors.New(errorMessageFilter)
}

func (r recordUCaseError) ReleaseFilter2() (release.Releases, error) {
	return release.Releases{}, errors.New(errorMessageFilter)
}

var xmlErrorResponse = fmt.Sprintf(`
<?xml version="1.0" encoding="UTF-8"?>
<error>
	<message>%s</message>
</error>
`, errorMessageFilter)

func TestHandlerFromFilterError(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	err := handlerFromFilter(recordUCaseError{}.ReleaseFilter1)(c)
	if err != nil {
		t.Errorf("error while trying to write to request: %v", err)
	}
	if rec.Code != http.StatusInternalServerError {
		t.Errorf("response code should be %d, got %d", http.StatusInternalServerError, rec.Code)
	}
	got := nonCharacterMatch.ReplaceAllString(rec.Body.String(), "")
	expected := nonCharacterMatch.ReplaceAllString(xmlErrorResponse, "")
	if got != expected {
		t.Errorf("response body should by %s, got %s instead", expected, got)
	}
}

func checkRouteContains(method, path string, routes []*echo.Route) bool {
	for _, route := range routes {
		if route.Method == method && route.Path == path {
			return true
		}
	}
	return false
}
