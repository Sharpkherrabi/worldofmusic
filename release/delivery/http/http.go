package http

import (
	"encoding/xml"
	"github.com/labstack/echo/v4"
	"gitlab.com/Sharpkherrabi/worldofmusic/domain/release"
	"net/http"
)

type recordUseCase interface {
	ReleaseFilter1() (release.Releases, error)
	ReleaseFilter2() (release.Releases, error)
}

type releaseHandler struct {
	recordUseCase recordUseCase
}

func NewReleaseHandler(e *echo.Echo, ruc recordUseCase) {
	handler := &releaseHandler{
		recordUseCase: ruc,
	}
	e.GET("/filter1", handler.filter1Handler())
	e.GET("/filter2", handler.filter2Handler())
}

func (rh releaseHandler) filter1Handler() func(ctx echo.Context) error {
	return handlerFromFilter(rh.recordUseCase.ReleaseFilter1)
}

func (rh releaseHandler) filter2Handler() func(ctx echo.Context) error {
	return handlerFromFilter(rh.recordUseCase.ReleaseFilter2)
}

func handlerFromFilter(f func() (release.Releases, error)) func(ctx echo.Context) error {
	return func(ctx echo.Context) error {
		releases, err := f()
		if err != nil {
			ctx.Logger().Error(err)
			return ctx.XML(http.StatusInternalServerError, responseError{Message: err.Error()})
		}
		return ctx.XML(http.StatusOK, releases)
	}
}

type responseError struct {
	XMLName xml.Name `xml:"error"`
	Message string   `xml:"message"`
}
