# syntax=docker/dockerfile:1.3

FROM --platform=${BUILDPLATFORM} golang:1.17.1-alpine AS base
WORKDIR /src
ENV CGO_ENABLED=0
COPY go.* .
RUN go mod download

FROM base AS build
ARG TARGETOS
ARG TARGETARCH
RUN --mount=target=. \
    GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -o /dist/worldofmusic .

FROM base AS unit-test
RUN --mount=target=. \
    mkdir /out && go test -v -coverprofile=/out/cover.out ./...

FROM golangci/golangci-lint:v1.31.0-alpine AS lint-base

FROM base AS lint
RUN --mount=target=. \
    --mount=from=lint-base,src=/usr/bin/golangci-lint,target=/usr/bin/golangci-lint \
    golangci-lint run --timeout 10m0s ./...

FROM scratch AS unit-test-coverage
COPY --from=unit-test /out/cover.out /cover.out

FROM scratch AS bin-unix
COPY --from=build /dist/worldofmusic /

FROM bin-unix AS bin-linux
FROM bin-unix AS bin-darwin

FROM scratch AS bin-windows
COPY --from=build /dist/worldofmusic /worldofmusic.exe

FROM bin-${TARGETOS} as bin