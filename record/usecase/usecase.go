package usecase

import (
	"fmt"
	"gitlab.com/Sharpkherrabi/worldofmusic/domain/record"
	"gitlab.com/Sharpkherrabi/worldofmusic/domain/release"
	"strings"
	"time"
)

type recordRepository interface {
	GetAll() ([]record.Record, error)
}

type recordUseCase struct {
	recordRepo recordRepository
}

// NewRecordUseCase initializes a recordUseCase instance using a recordRepo.
func NewRecordUseCase(recordRepo recordRepository) *recordUseCase {
	return &recordUseCase{
		recordRepo: recordRepo,
	}
}

// ReleaseFilter1 returns a list of releases with more 10 tracks and a release date before 01/01/2001.
func (ruc recordUseCase) ReleaseFilter1() (release.Releases, error) {
	records, err := ruc.recordRepo.GetAll()
	if err != nil {
		return release.Releases{}, fmt.Errorf("could not fetch the list of records: %w", err)
	}
	nOfTracks := 10
	records, err = RecordsWithMoreThanNTracks(records, nOfTracks)
	if err != nil {
		return release.Releases{}, fmt.Errorf("could not get releases with more than %d tracks: %w",
			nOfTracks, err)
	}
	layout := "2006.01.02"
	dateString := "2001.01.01"
	t, err := time.Parse(layout, dateString)
	if err != nil {
		return release.Releases{}, fmt.Errorf("could not parse the date String %s using layout %s: %w",
			dateString, layout, err)
	}
	records, err = RecordsBeforeN(records, t)
	if err != nil {
		return release.Releases{}, err
	}
	return record.RecordsList(records).ToReleases(), nil
}

// ReleaseFilter2 returns a list of releases with at least 2 Compact Discs (CDs).
func (ruc recordUseCase) ReleaseFilter2() (release.Releases, error) {
	records, err := ruc.recordRepo.GetAll()
	if err != nil {
		return release.Releases{}, fmt.Errorf("could not fetch the list of records: %w", err)
	}
	nOfTracks := 1
	records, err = RecordsWithMoreThanNTracks(records, nOfTracks)
	if err != nil {
		return release.Releases{}, fmt.Errorf("could not get releases with more than %d tracks: %w",
			nOfTracks, err)
	}
	records = RecordsWithFormatFs(records, "CD")
	return record.RecordsList(records).ToReleases(), nil
}

// RecordsWithMoreThanNTracks takes a list of record.Record and returns a list of Records with
// a number of Tracks greater than n.
func RecordsWithMoreThanNTracks(records []record.Record, n int) (record.RecordsList, error) {
	if n < 0 {
		return record.RecordsList{}, fmt.Errorf("number of tracks must be greater equal 0, got: %d", n)
	}
	var recordsList record.RecordsList
	for _, rd := range records {
		if len(rd.Tracks) > n {
			recordsList = append(recordsList, rd)
		}
	}
	return recordsList, nil
}

// RecordsBeforeN takes a list of record.Record and returns a list of Records with
// a release date before n.
func RecordsBeforeN(records []record.Record, n time.Time) (record.RecordsList, error) {
	if n.After(time.Now()) {
		return record.RecordsList{}, fmt.Errorf("n must be in the past")
	}
	var recordsList record.RecordsList
	for _, rd := range records {
		if rd.ReleaseDate.Before(n) {
			recordsList = append(recordsList, rd)
		}
	}
	return recordsList, nil
}

// RecordsWithFormatFs takes a list of record.Record and returns a list of Records with
// format fs.
func RecordsWithFormatFs(records []record.Record, fs ...string) record.RecordsList {
	var recordsList record.RecordsList
	for _, rd := range records {
		for _, format := range fs {
			if insensitiveContains(rd.Formats, format) {
				recordsList = append(recordsList, rd)
				break
			}
		}
	}
	return recordsList
}

func insensitiveContains(s []string, e string) bool {
	for _, a := range s {
		if strings.EqualFold(a, e) {
			return true
		}
	}
	return false
}
