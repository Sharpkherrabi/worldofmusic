package usecase

import (
	"gitlab.com/Sharpkherrabi/worldofmusic/domain/record"
	"reflect"
	"testing"
	"time"
)

func TestInsensitiveContains(t *testing.T) {
	var tests = []struct {
		input struct {
			list  []string
			value string
		}
		want bool
	}{
		{
			struct {
				list  []string
				value string
			}{
				list:  []string{"alfa", "gama", "beta"},
				value: "BeTa"},
			true,
		},
		{
			struct {
				list  []string
				value string
			}{
				list:  []string{"alfa", "gama", "beta"},
				value: "BETA"},
			true,
		},
		{
			struct {
				list  []string
				value string
			}{
				list:  []string{"alfa", "gama", "beta"},
				value: "beta"},
			true,
		},
		{
			struct {
				list  []string
				value string
			}{
				list:  []string{"alfa", "gama", "beta"},
				value: "bota"},
			false,
		},
	}
	for _, test := range tests {
		if got := insensitiveContains(test.input.list, test.input.value); got != test.want {
			t.Errorf("insensitiveContains(%+v, %s) = %v", test.input.list, test.input.value, got)
		}
	}
}

var layout = "2006.01.02"
var records = []record.Record{
	{
		Title:       "Good Vibes",
		Name:        "Good Vibes Summer",
		Genre:       "HipHop",
		ReleaseDate: time.Date(2000, time.February, 1, 0, 0, 0, 0, time.UTC),
		Label:       "Luxe",
		Formats:     []string{"CD", "Cassette", "DVD"},
		Tracks:      []string{"Take me to church", "Alpine", "Red Flags", "Apocalypse"},
	},
	{
		Title:       "Blue Screen",
		Name:        "Blue Screen Winter",
		Genre:       "Rock&Roll",
		ReleaseDate: time.Date(1999, time.December, 1, 0, 0, 0, 0, time.UTC),
		Label:       "Favorite",
		Formats:     []string{"Cassette", "DVD"},
		Tracks:      []string{"Fly higher than the sky", "Frozen Heart", "Seniorita"},
	},
	{
		Title:       "Bad Habits",
		Name:        "Bad Habits Winter",
		Genre:       "R&B",
		ReleaseDate: time.Date(1999, time.December, 31, 0, 0, 0, 0, time.UTC),
		Label:       "Daemon",
		Formats:     []string{"Cassette"},
		Tracks: []string{"Track1", "Track2", "Track3", "Track4",
			"Track5", "Track6", "Track7", "Track8", "Track9", "Track10", "Track11"},
	},
	{
		Title:       "Alfa Domeno",
		Name:        "Alfa Domeno alcatra",
		Genre:       "R&B",
		ReleaseDate: time.Date(2001, time.January, 1, 0, 0, 0, 0, time.UTC),
		Label:       "Angel",
		Formats:     []string{"Cassette", "CD"},
		Tracks: []string{"Track1", "Track2", "Track3", "Track4",
			"Track5", "Track6", "Track7", "Track8", "Track9", "Track10", "Track11", "Track12"},
	},
}

func TestRecordsWithFormatFs(t *testing.T) {
	recordsWithCDFormat := record.RecordsList([]record.Record{records[0], records[3]})
	format := "cd"

	if got := RecordsWithFormatFs(records, format); !reflect.DeepEqual(got, recordsWithCDFormat) {
		t.Errorf("got %+v, \nwant %+v", got, recordsWithCDFormat)
	}
}

func TestRecordsBeforeN(t *testing.T) {
	recordsBeforeJanuary2000 := record.RecordsList([]record.Record{records[1], records[2]})
	tm, _ := time.Parse(layout, "2000.01.01")
	got, err := RecordsBeforeN(records, tm)
	if err != nil {
		t.Errorf("error occurred while calling RecordsBeforeN(%+v, %s): %v", records, tm.String(), err)
	}
	if !reflect.DeepEqual(got, recordsBeforeJanuary2000) {
		t.Errorf("got %+v, \nwant %+v", got, recordsBeforeJanuary2000)
	}
}

func TestRecordsBeforeNError(t *testing.T) {
	tm := time.Now().Add(time.Second)
	_, err := RecordsBeforeN(records, tm)
	if err == nil {
		t.Error("expected error not be nil")
	}
}

func TestRecordsWithMoreThanNTracks(t *testing.T) {
	recordsWithMoreThan5Tracks := record.RecordsList([]record.Record{records[2], records[3]})
	NOfTracks := 5
	got, err := RecordsWithMoreThanNTracks(records, NOfTracks)
	if err != nil {
		t.Errorf("error occurred while calling RecordsWithMoreThanNTracks(%+v, %d): %v", records, NOfTracks, err)
	}
	if !reflect.DeepEqual(got, recordsWithMoreThan5Tracks) {
		t.Errorf("got %+v, \nwant %+v", got, recordsWithMoreThan5Tracks)
	}
}

func TestRecordsWithMoreThanNTracksError(t *testing.T) {
	nOfTracks := -1
	_, err := RecordsWithMoreThanNTracks(records, nOfTracks)
	if err == nil {
		t.Error("error expected to not be nil")
	}
}

type recordRepo struct{}

func (r recordRepo) GetAll() ([]record.Record, error) {
	return records, nil
}

func TestRecordUseCase_ReleaseFilter1(t *testing.T) {
	filteredReleases := record.RecordsList([]record.Record{records[2]}).ToReleases()
	ruc := NewRecordUseCase(recordRepo{})
	got, err := ruc.ReleaseFilter1()
	if err != nil {
		t.Errorf("error occurred while calling ReleaseFilter1(): %v", err)
	}
	if !reflect.DeepEqual(got, filteredReleases) {
		t.Errorf("got %+v, \nwant %+v", got, filteredReleases)
	}
}

func TestRecordUseCase_ReleaseFilter2(t *testing.T) {
	filteredReleases := record.RecordsList([]record.Record{records[0], records[3]}).ToReleases()
	ruc := NewRecordUseCase(recordRepo{})
	got, err := ruc.ReleaseFilter2()
	if err != nil {
		t.Errorf("error occurred while calling ReleaseFilter2(): %v", err)
	}
	if !reflect.DeepEqual(got, filteredReleases) {
		t.Errorf("got %+v, \nwant %+v", got, filteredReleases)
	}
}
