package xml

import (
	"encoding/xml"
	"gitlab.com/Sharpkherrabi/worldofmusic/domain/record"
	"io"
	"io/ioutil"
	"sync"
)

var once sync.Once
var records []record.Record

type xmlRecordRepository struct {
	reader io.Reader
}

// NewRecordRepository takes source as a data source and returns a xmlRecordRepository instance.
func NewRecordRepository(source io.Reader) *xmlRecordRepository {
	return &xmlRecordRepository{
		source,
	}
}

// GetAll reads all bytes from the source and returns a list record.Record.
func (p *xmlRecordRepository) GetAll() (rs []record.Record, err error) {
	once.Do(func() {
		var model record.XMLRecords
		b, readErr := ioutil.ReadAll(p.reader)
		if readErr != nil {
			err = readErr
			return
		}
		unmarshalErr := xml.Unmarshal(b, &model)
		if unmarshalErr != nil {
			err = unmarshalErr
			return
		}
		records = model.ToRecords()
	})
	rs = records
	return
}
