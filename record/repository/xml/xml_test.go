package xml

import (
	"fmt"
	"gitlab.com/Sharpkherrabi/worldofmusic/domain/record"
	"reflect"
	"strings"
	"sync"
	"testing"
	"time"
)

var xmlString = `
<records>
	<record>
		<title>Doggy Moggy</title>
		<name>Doggy Moggy vol.1</name>
		<genre>Comedy</genre>
		<releasedate>2005.07.26</releasedate>
		<label>Folkways</label>
		<formats>CD</formats>
		<tracklisting>
			<track>Henry Berta</track>
			<track>Flower Romance</track>
			<track>Wild Wind</track>
			<track>Darkness</track>
			<track>First Lady</track>
		</tracklisting>
	</record>
	<record>
		<title>Dancing Flowers</title>
		<name>Dancing Flowers vol.5</name>
		<genre>Rap</genre>
		<releasedate>2004.03.27</releasedate>
		<label>Smithsonian</label>
		<formats>CD, Cassette</formats>
		<tracklisting>
			<track>Henry Boy</track>
			<track>Lucy Alphonso</track>
			<track>David Albert</track>
		</tracklisting>
	</record>
</records>`

func recordsList() []record.Record {
	releaseDate1, _ := time.Parse("2006.01.02", "2005.07.26")
	releaseDate2, _ := time.Parse("2006.01.02", "2004.03.27")
	return []record.Record{
		{
			Title:       "Doggy Moggy",
			Name:        "Doggy Moggy vol.1",
			Genre:       "Comedy",
			ReleaseDate: releaseDate1,
			Label:       "Folkways",
			Formats:     []string{"CD"},
			Tracks: []string{
				"Henry Berta",
				"Flower Romance",
				"Wild Wind",
				"Darkness",
				"First Lady",
			},
		},
		{
			Title:       "Dancing Flowers",
			Name:        "Dancing Flowers vol.5",
			Genre:       "Rap",
			ReleaseDate: releaseDate2,
			Label:       "Smithsonian",
			Formats:     []string{"CD", "Cassette"},
			Tracks: []string{
				"Henry Boy",
				"Lucy Alphonso",
				"David Albert",
			},
		},
	}
}

func TestXmlRecordRepository_GetAll(t *testing.T) {
	once = sync.Once{}
	repository := NewRecordRepository(strings.NewReader(xmlString))
	records, err := repository.GetAll()
	if err != nil {
		t.Fatalf("XML unmarshalling failed: %v", err)
	}
	if !reflect.DeepEqual(records, recordsList()) {
		t.Errorf("expected %v, \ngot %v", recordsList(), records)
	}
}

var nonValidXmlString = `
<root>
     <element1>A</element1>
     <element2>B</element2>
</root>
`

func TestXmlRecordRepository_GetAllError(t *testing.T) {
	once = sync.Once{}
	repository := NewRecordRepository(strings.NewReader(nonValidXmlString))
	_, err := repository.GetAll()
	if err == nil {
		fmt.Println(err)
		t.Errorf("an error should be returned, instead got: %v", err)
	}
}
