package main

import (
	"bytes"
	"context"
	_ "embed"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"gitlab.com/Sharpkherrabi/worldofmusic/record/repository/xml"
	"gitlab.com/Sharpkherrabi/worldofmusic/record/usecase"
	releaseHttpDelivery "gitlab.com/Sharpkherrabi/worldofmusic/release/delivery/http"
	"net/http"
	"os"
	"os/signal"
	"time"
)

//go:embed resources/worldofmusic.xml
var XMLFile []byte

func main() {
	e := echo.New()
	e.Logger.SetLevel(log.INFO)

	recordRepo := xml.NewRecordRepository(bytes.NewReader(XMLFile))
	recordUCase := usecase.NewRecordUseCase(recordRepo)
	releaseHttpDelivery.NewReleaseHandler(e, recordUCase)

	// HTTP Server Configuration
	PORT, set := os.LookupEnv("PORT")
	if !set {
		PORT = "8080"
	}
	s := &http.Server{
		Addr:         fmt.Sprintf(":%s", PORT),
		Handler:      e,
		IdleTimeout:  120 * time.Second,
		ReadTimeout:  1 * time.Second,
		WriteTimeout: 1 * time.Second,
	}
	go func() {
		err := s.ListenAndServe()
		if err != http.ErrServerClosed {
			e.Logger.Fatalf("Could not start the server: %v\n", err)
		}
	}()
	// Shutdown the server gracefully and wait for open http connections to close 6 Seconds
	// after the process receives an Interrupt or Kill signal.
	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill)
	defer stop()
	<-ctx.Done()
	e.Logger.Info("Shutting down the server...")
	timeoutCtx, cancel := context.WithTimeout(context.Background(), 6*time.Second)
	defer cancel()
	if err := s.Shutdown(timeoutCtx); err != nil {
		e.Logger.Fatal("The server could not shutdown gracefully")
	}
}
